# Pi

Various Raspberry Pi common code.

## Setup

The `ansible/` directory contains Ansible Playbooks and Roles for setting
up and maintaining a Raspberry Pi. They expect Raspbian, or at least a Debian based OS,
to have been installed on the Pi. It also presumes you know the login for the default `pi`
account. If the typical `pi` account is unavailable, make sure you substitute it or overwrite it
with the correct user.
